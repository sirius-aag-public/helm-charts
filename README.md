# helm-charts

GitLab takes all files of the `public` folder and publishes them at https://sirius-aag-public.gitlab.io/helm-chart.

To use your new Chart repository, run: 
```
$ helm repo add my-repo https://sirius-aag-public.gitlab.io/helm-chart
```
on your local computer.

